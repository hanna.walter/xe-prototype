import { css } from "emotion";
import React from "react";

const Container = ({ children, components, data, ...props }) => {
  const {
    height = "100%",
    width = "100%",
    minWidth,
    maxWidth,
    wrap,
    verticalAlign,
    justifyContent,
    maxHeight,
    minHeight,
    containers = []
  } = data;

  return (
    <div
      id={data.id}
      className={css`
        display: flex;
        height: ${height};
        width: ${width};
        min-width: ${minWidth};
        max-width: ${maxWidth};
        max-height: ${maxHeight};
        min-height: ${minHeight};
        border: 1px solid hotpink;
        align-self: ${getVerticalAlign(verticalAlign)};
        justify-content: ${getJustify(justifyContent)};
        padding: 5px;
        margin: 0px;
        flex-wrap: ${wrap ? "wrap" : "nowrap"};
        box-sizing: border-box;
        background-color: black;
      `}
    >
      {components[data.id]}
      {containers.map(child => (
        <Container
          key={child.id}
          data={child}
          children={child.children}
          components={components}
        />
      ))}
    </div>
  );
};

function getVerticalAlign(verticalAlign) {
  return verticalAlign === "center"
    ? "center"
    : verticalAlign === "bottom"
    ? "flex-end"
    : "flex-start";
}

function getJustify(justify) {
  return justify === "right"
    ? "flex-end"
    : justify === "center"
    ? "center"
    : justify === "between"
    ? "space-between"
    : justify === "evenly"
    ? "space-evenly"
    : "flex-start";
}

export default Container;
