import React from "react";
import Container from "./Container";
import spec from "./spec2";
import "./styles.css";

export default function App() {
  const button = (
    <div>
      <button>I'm a button</button>
    </div>
  );
  const otherButton = (
    <div>
      <button>I'm a different button</button>
    </div>
  );
  const componentObject = {
    "dsfhjkl-dfs-dsmnbfn": [button, button, button, button],
    "dsfhjkl-dfs-cxbv": otherButton
  };

  /*
  So there's a few ways we can attach the content into the layout
  1) Like above, use a key/value pairing that maps with the the IDs that we pass down
  2) Modify the spec and inject the children onto it
  3) Possibly using portals (I haven't gotten this to work yet)
  4) Have every container "fetch" its own children
  */
  return (
    <div className="App">
      <Container data={spec.container} components={componentObject} />
    </div>
  );
}
