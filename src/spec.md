# Spec

id: string; [undefined]
minWidth: string; [none]
maxWidth: string; [none]
width: string; [100%]
minHeight: string; [none]
maxHeight: string; [none]
height: string; [100%]
wrap: boolean [false]
verticalAlign: "top" | "bottom" | "center" [top]
justifyContent: "left" | "right" | "center" | "between" | "evenly"; [left]
containers: this[]; [undefined]
